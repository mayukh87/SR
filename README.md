# Speech Recognition

## Requirements

In summary, this library requires:
* Python 2.6, 2.7, or 3.3+
* PyAudio 0.2.9+ (required only if you need to use microphone input)

## Setup
* pip install SpeechRecognition
* sudo port install py27-pyaudio
* python -c "import pyaudio"
* pip install --global-option='build_ext' --global-option='-I/opt/local/include' --global-option='-L/opt/local/lib' pyaudio --upgrade